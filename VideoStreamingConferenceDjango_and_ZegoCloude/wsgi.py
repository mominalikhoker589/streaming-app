"""
WSGI config for VideoStreamingConferenceDjango_and_ZegoCloude project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'VideoStreamingConferenceDjango_and_ZegoCloude.settings')

application = get_wsgi_application()
