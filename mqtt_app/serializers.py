from rest_framework import serializers



class PublishMessageSerializer(serializers.Serializer):
    topic = serializers.CharField(max_length=255)
    msg = serializers.CharField(max_length=255)
