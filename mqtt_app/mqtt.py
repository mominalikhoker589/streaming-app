import paho.mqtt.client as mqtt
import requests
import sys
import time


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    topic = 'SENSOR/+'

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(topic)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    """
    The on_message callback takes 3 arguments:
    1 : client
    2 : userdata
    3 : msg

    The data sent from broker is stored in the 'msg' argument.
    It containes both the 'topic' the data was received from,
    and the payload.

    We parse the topic to get the id of the sensor.
    And we get the payload from msg.payload as well.

    We then send this data to our API.

    You can create a database connection in this file, and instead
    store the data directly in the database, that would be
    preferred in production environments.
    """
    print(msg.topic + " " + str(msg.payload))
    url = "http://localhost:8000/sensors/readings/"
    id = int(msg.topic.split("/")[1])
    print(id)

    data = {
        "sensor": id,
        "reading": msg.payload
    }
    requests.post(url, data)


# Connection to the client is established here
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("127.0.0.1", port=1883)
client.loop_forever()

import paho.mqtt.client as mqtt
from random import randrange, uniform, random
import math
import time

mqttBroker = "127.0.0.1"  # Our mosquitto broker running on 127.0.0.1
client = mqtt.Client()  # instantiating the mqtt client.

client.connect(mqttBroker, port=1883)  # Connecting mqtt client to broker.
i = 0
# The loop just sends random sin values to a topic in broker.
while i < 2000:
    num = math.sin(i)  # Sine value of i
    # id = randrange(1,5)
    id = randrange(4, 8)  # Id of our sensor in backend.
    client.publish(f"SENSOR/{id}", num)  # Publish the number to topic 'Sensor/id_of_sensor'
    print(f"Just published {num} to topic SENSOR/{id} with count {i}")
    i = i + 1
    time.sleep(0.5)

# mqtt_app/mqtt.py
# import paho.mqtt.client as mqtt
# import json
# from django.conf import settings
# import requests
# from rest_framework import status
# from rest_framework.response import Response

# received_messages = []


# def on_connect(mqtt_client, userdata, flags, rc):
#     if rc == 0:
#         print('Connected successfully')
#         mqtt_client.subscribe('django/mqtt')
#     else:
#         print('Bad connection. Code:', rc)


# def on_message(mqtt_client, userdata, msg):
#     print(f'Received message on topic: {msg.topic} with payload: {msg.payload}')
# data = f'Received message on topic: {msg.topic} with payload: {msg.payload}'
# try:
#     payload_dict = json.loads(msg.payload)
#     received_messages.append(payload_dict)
# except json.JSONDecodeError as e:
#     print('Error decoding JSON payload:', e)
# try:
#     payload_dict = json.loads(msg.payload)
#     operation = payload_dict.get('operation')
#
#     if operation == 'create':
#         create_message(payload_dict)
#     elif operation == 'read':
#         read_message(payload_dict)
#     elif operation == 'update':
#         update_message(payload_dict)
#     elif operation == 'delete':
#         delete_message(payload_dict)
#     else:
#         Response({"message": f"Unknown operation {operation}"}, status=status.HTTP_400_BAD_REQUEST)
#         print('Unknown operation:', operation)
#
# except json.JSONDecodeError as e:
#     print('Error decoding JSON payload:', e)
#     Response({"error": f"Error decoding JSON payload: {e}"}, status=status.HTTP_400_BAD_REQUEST)


# def create_message(payload_dict):
#     topic = payload_dict.get('topic')
#     content = payload_dict.get('content')
#
#     if topic and content:
#         requests.post(f'http://localhost:8000/api/messages/', {'topic': topic, 'content': content})
#         print('Message created successfully')
#         Response({"message": f"Message created successfully."}, status=status.HTTP_201_CREATED)
#     else:
#         print('Invalid payload for create operation')
#         Response({"message": f"Invalid payload."}, status=status.HTTP_400_BAD_REQUEST)
#
#
# def read_message(payload_dict):
#     topic = payload_dict.get('topic')
#
#     if topic:
#         response = requests.get(f'http://localhost:8000/api/messages/?topic={topic}')
#         data = response.json()
#         Response({"message": f"Read message.: {data}"}, status=status.HTTP_200_OK)
#         print('Read message:', data)
#     else:
#         print('Invalid payload for read operation')
#         Response({"message": f"Invalid payload."}, status=status.HTTP_404_NOT_FOUND)
#
#
# def update_message(payload_dict):
#     topic = payload_dict.get('topic')
#     content = payload_dict.get('content')
#
#     if topic and content:
#         response = requests.get(f'http://localhost:8000/api/messages/?topic={topic}')
#         data = response.json()
#
#         if data:
#             message_id = data[0]['id']
#             requests.put(f'http://localhost:8000/api/messages/{message_id}/', {'content': content})
#             print('Message updated successfully')
#             Response({"message": f"Message updated successfully"}, status=status.HTTP_200_OK)
#         else:
#             print('Message not found for update operation')
#             Response({"message": f"Message not found."}, status=status.HTTP_404_NOT_FOUND)
#     else:
#         print('Invalid payload for update operation')
#         Response({"message": f"Invalid payload."}, status=status.HTTP_400_BAD_REQUEST)
#
#
# def delete_message(payload_dict):
#     topic = payload_dict.get('topic')
#
#     if topic:
#         response = requests.get(f'http://localhost:8000/api/messages/?topic={topic}')
#         data = response.json()
#
#         if data:
#             message_id = data[0]['id']
#             requests.delete(f'http://localhost:8000/api/messages/{message_id}/')
#             print('Message deleted successfully')
#             Response({"message": f"Message deleted successfully."}, status=status.HTTP_204_NO_CONTENT)
#         else:
#             print('Message not found for delete operation')
#             Response({"message": f"Message not found."}, status=status.HTTP_404_NOT_FOUND)
#     else:
#         print('Invalid payload for delete operation')
#         Response({"message": f"Id is not Valid."}, status=status.HTTP_404_NOT_FOUND)


# client = mqtt.Client()
# client.on_connect = on_connect
# client.on_message = on_message
# client.username_pw_set(settings.MQTT_USER, settings.MQTT_PASSWORD)
# client.connect(
#     host=settings.MQTT_SERVER,
#     port=settings.MQTT_PORT,
#     keepalive=settings.MQTT_KEEPALIVE
# )


# # mqtt_app/mqtt.py
# import paho.mqtt.client as mqtt
#
#
# class MqttClient:
#     def __init__(self, broker_address, port=1883):
#         self.client = mqtt.Client()
#         self.client.on_connect = self.on_connect
#         self.client.on_message = self.on_message
#         self.broker_address = broker_address
#         self.port = port
#
#     def on_connect(self, client, userdata, flags, rc):
#         print(f"Connected with result code {rc}")
#         client.subscribe("your/topic")
#
#     def on_message(self, client, userdata, msg):
#         print(f"Received message: {msg.payload.decode()}")
#
#     def connect(self):
#         self.client.connect(self.broker_address, self.port, 60)
#         self.client.loop_start()
#
#     def publish(self, topic, message):
#         self.client.publish(topic, message)
