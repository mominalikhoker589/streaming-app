from django import forms
from .models import MqttData


class MqttDataForm(forms.ModelForm):
    class Meta:
        model = MqttData
        fields = ['topic', 'message']
