import paho.mqtt.client as mqtt

import time


def on_message(client, userdata, message):
    print("received message", str(message.payload.decode("utf-8")))
    print("received topic=", message.topic)
    print("received qos=", message.qos)
    print("received retain flag", message.retain)


print("creating a new instance")
print("creating a new instance")
print("creating a new instance")
print("creating a new instance")
client = mqtt.Client()
print("calling a callback function")
client.on_message = on_message
client.connect("mqtt-dashboard.com")
client.loop_start()
# client.subscribe('room/ac/')
# client.publish("room/ac/", "AC OFF!")
time.sleep(4)
client.loop_stop()

from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from mqtt_app.forms import MqttDataForm


@csrf_exempt
def publish_message(request):
    if request.method == 'POST':
        form = MqttDataForm(request.POST)
        if form.is_valid():
            client.subscribe(form.topic)
            client.publish()
            form.save()
            return {"message": "sucess"}
        #     return render(request, 'login.html', {'success': "Registration successful. Please login."})
        # else:
        #     error_message = form.errors.as_text()
        #     return render(request, 'register.html', {'error': error_message})

    return {"message": "sucess"}


def mqtt_scrape(request):
    if request.method == 'POST':
        form = MqttDataForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = MqttDataForm()

    mqtt_client = mqtt.Client()

    def on_message(client, userdata, msg):
        MqttData.objects.create(topic=msg.topic, message=msg.payload.decode())

    mqtt_client.on_message = on_message
    mqtt_client.connect("mqtt.eclipse.org", 1883, 60)
    mqtt_client.subscribe("django/topic", qos=1)
    mqtt_client.loop_start()

    return render(request, 'mqtt_scrape.html', {'form': form})
