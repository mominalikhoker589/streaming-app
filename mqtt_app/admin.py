from django.contrib import admin

from mqtt_app.models import MqttData

# Register your models here.

admin.site.register(MqttData)
