from django.apps import AppConfig


class VideostreamingconferenceAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'videostreamingconference_app'
